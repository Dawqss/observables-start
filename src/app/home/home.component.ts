import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import {Observer} from "rxjs/Observer";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  numbersObsSubscription: Subscription;
  customObsSubscription: Subscription;
  constructor() { }

  ngOnInit() {
    const myNumbers = Observable.interval(1000);
    this.numbersObsSubscription = myNumbers.subscribe(
        (number: number) => {
          console.log(number);
        }
    );

    const myObservable =  Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        observer.next('first packacge');
      },2000);
      setTimeout(() => {
          observer.next('second packacge');
      },4000);
      setTimeout(() => {
          observer.complete();
      },5000);
        setTimeout(() => {
            observer.next('third packacge');
        },7000);
    });

    this.customObsSubscription = myObservable.subscribe(
        (data: string) => {
          console.log(data);
        },
        (error: string) => {
          console.log(error);
        },
        () => {console.log('completed')}
        );
    }

    ngOnDestroy() {
      this.numbersObsSubscription.unsubscribe();
      this.customObsSubscription.unsubscribe();
    }
}
